package com.sc.myrss.web.mvc.rss;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Tree implements Serializable {

	private static final long serialVersionUID = -8965797841651959574L;

	private List<TreeItem> items = new ArrayList<TreeItem>();

	public List<TreeItem> getItems() {
		return items;
	}

	public void setItems(List<TreeItem> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Tree [items=" + items + "]";
	}

}
