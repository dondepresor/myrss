package com.sc.myrss.neg.rss.aggregator;

public interface AggregatorManager {

	public void start();

	public void stop();

	public void configure();

}
