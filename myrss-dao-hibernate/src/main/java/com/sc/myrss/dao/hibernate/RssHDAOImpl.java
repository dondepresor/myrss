package com.sc.myrss.dao.hibernate;

import java.util.List;

import com.sc.myrss.dao.RssDAO;
import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;

public class RssHDAOImpl extends BaseHDAO implements RssDAO {

	@Override
	public List<Subscription> getUsersSubscriptions(String userId) {
		return runListQuery(
			"SELECT subscription "+
			"FROM Subscription subscription "+
			"WHERE subscription.userId = :userId",
			Subscription.class,
			"userId", userId);
	}

	@Override
	public Integer countSubscriptionPendingArticles(String userId, String url) {
		return runSingleSelectQuery(
			"SELECT COUNT(a) "+
			"FROM Article a "+
			"WHERE a.userId = :userId "+
			"AND a.subscriptionUrl = :url AND a.read = 0", 
			Long.class,
			"userId", userId,
			"url", url).intValue();
	}

	@Override
	public Integer countCategoryPendingArticles(String userId, String category) {
		return runSingleSelectQuery(
			"SELECT COUNT(a) "+
			"FROM Article a "+
			"LEFT OUTER JOIN Subscription s "+
				"ON (a.userId = s.userId AND a.subscriptionUrl = s.url) "+
			" WHERE s.userId = :userId "+
			"AND s.category = :category "+
			"AND a.read = 0",
			Long.class,
			"userId", userId,
			"category", category).intValue();
	}

	@Override
	public Integer countPendingArticles(String userId) {
		return runSingleSelectQuery(
			"SELECT COUNT(a) "+
			"FROM Article a "+
			"WHERE a.userId = :userId "+
			"AND a.read = 0",
			Long.class,
			"userId", userId).intValue();
	}

	@Override
	public Subscription getSubscription(String userId, String url) {
		return runSingleSelectQuery(
			"SELECT s FROM Subscription s "+
			"WHERE s.userId = :userId "+
			"AND s.url = :url",
			Subscription.class,
			"userId", userId,
			"url", url);
	}

	@Override
	public List<Long> getUserArticleIds(String userId, boolean unreadOnly) {
		return runListQuery(
			"SELECT a.readId "+
			"FROM Article a "+
			"WHERE (a.userId, a.subscriptionUrl) IN ("+
				"SELECT s.userId, s.url "+
				"FROM Subscription s "+
				"WHERE s.userId = :login "+
			")"+
			(unreadOnly? " AND a.read = 0 ": "")+
			"ORDER BY a.publicationDate DESC",
			Long.class,
			"login", userId
		);
	}

	@Override
	public List<Long> getSubscriptionArticleIds(String userId, String url, boolean unreadOnly) {
		return runListQuery(
			"SELECT a.readId "+
			"FROM Article a "+
			"WHERE a.userId = :login "+
			"AND a.subscriptionUrl = :url "+
			(unreadOnly? " AND a.read = 0 ": "")+
			"ORDER BY a.publicationDate DESC",
			Long.class,
			"login", userId,
			"url", url);
	}

	@Override
	public List<Long> getCategoryArticleIds(String userId, String category, boolean unreadOnly) {
		return runListQuery(
			"SELECT a.readId "+
			"FROM Article a "+
			"WHERE (a.userId, a.subscriptionUrl) IN ("+
				"SELECT s.userId, s.url "+
				"FROM Subscription s "+
				"WHERE s.userId = :login "+
				"AND s.category = :category "+
			")"+
			(unreadOnly? " AND a.read = 0 ": "")+
			"ORDER BY a.publicationDate DESC",
			Long.class,
			"login", userId,
			"category", category);
	}

	@Override
	public List<Article> getArticlesByIds(List<Long> ids) {
		return runListQuery(
			"SELECT a "+
			"FROM Article a "+
			"WHERE a.readId in :ids",
			Article.class,
			"ids", ids);
	}

	@Override
	public void markArticleAsRead(Long readId, boolean readValue) {
		runUpdateQuery(
			"UPDATE Article a "+
			"SET a.read = :readValue "+
			"WHERE a.readId = :readId ",
			"readId", readId,
			"readValue", readValue);
	}

	@Override
	public void markSubscriptionAsRead(String userId, String url, boolean readValue) {
		runUpdateQuery(
			"UPDATE Article a "+
			"SET a.read = :readValue "+
			"WHERE a.userId = :userId "+
			"AND a.subscriptionUrl = :url "+
			"AND a.read = 0",
			"userId", userId,
			"url", url,
			"readValue", readValue);
	}

	@Override
	public void markCategoryAsRead(String userId, String categoryName, boolean readValue) {
		runUpdateQuery(
			"UPDATE Article a "+
			"SET a.read = :readValue "+
			"WHERE (a.userId, a.subscriptionUrl) IN ("+
				"SELECT s.userId, s.url "+
				"FROM Subscription s "+
				"WHERE s.userId = :userId "+
				"AND s.category = :category"+
			")",
			"userId", userId,
			"category", categoryName,
			"readValue", readValue);
	}

	public void markUserAsRead(String userId, boolean readValue) {
		runUpdateQuery(
			"UPDATE Article a "+
			"SET a.read = :readValue "+
			"WHERE (a.userId, a.subscriptionUrl) IN ("+
				"SELECT s.userId, s.url "+
				"FROM Subscription s "+
				"WHERE s.userId = :userId "+
			")",
			"userId", userId,
			"readValue", readValue);
	}
}
