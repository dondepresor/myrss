package com.sc.myrss.web.mvc.rss;

import java.io.Serializable;

public class ItemForm implements Serializable {

	private static final long serialVersionUID = -7533253550404718526L;

	private String type;

	private String category;

	private String feed;

	private boolean unreadOnly = true;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public Boolean getUnreadOnly() {
		return unreadOnly;
	}

	public void setUnreadOnly(Boolean unreadOnly) {
		this.unreadOnly = unreadOnly;
	}

	@Override
	public String toString() {
		return "ItemForm [type=" + type + ", category=" + category + ", feed=" + feed + ", unreadOnly=" + unreadOnly
				+ "]";
	}

}