package com.sc.myrss.web.security;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sc.myrss.model.security.User;
import com.sc.myrss.web.WebConstants;
import com.sc.myrss.web.security.exception.AuthenticationException;
import com.sc.myrss.web.security.exception.AuthorizationException;
import com.sc.myrss.web.security.exception.NoContentException;

@Component("securityFilterHttp")
public class SecurityFilterHttp implements Filter {

	private static final String SECURITY_RESOURCE_PREFIX_PROPERTY_NAME = "security-resources-prefix";

	private static final String LOGIN_FORM_URL_PROPERTY_NAME = "login-form-url";

	private static final String PUBLIC_RESOURCE = "public";

	@Autowired
	Properties securityResources;

	private String securityResourcesPrefix;

	private String loginFormUrl;

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.securityResourcesPrefix = config.getInitParameter(SECURITY_RESOURCE_PREFIX_PROPERTY_NAME);
		this.loginFormUrl = config.getInitParameter(LOGIN_FORM_URL_PROPERTY_NAME);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (!(request instanceof HttpServletRequest)) {
			return;
		}
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		if (httpRequest == null || httpResponse == null || httpRequest.getRequestURI() == null) {
			return;
		}
		
		try {
			HttpSession httpSession = httpRequest.getSession(true);
			
			String url = httpRequest.getRequestURI();
			ActionRequest actionRequest = getActionRequestFromUrl(url);
			if(actionRequest==null) {
				throw new NoContentException();
			} else if(PUBLIC_RESOURCE.equals(actionRequest.getSecResource())) {
				chain.doFilter(request, response);
			} else {
				User user = (User)httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME);
				if(user==null) {
					throw new AuthenticationException();
				}
				if(user.can(actionRequest.getSecResource())) {
					if("/".equals(actionRequest.getAction())) {
						request.getRequestDispatcher("/index.html").forward(request, response);
					} else {
						chain.doFilter(request, response);
					}
				} else {
					throw new AuthorizationException();
				}
			}
		} catch(NoContentException e) {
			// 204 No content -The server successfully processed the request and is not returning any content.
			httpResponse.sendError(204, "No content");
		} catch (AuthenticationException e) {
			// 401 - Unauthorized - authentication is required and has failed or has not yet been provided
			// httpResponse.sendError(401, "User Not Authenticated");
			httpResponse.sendRedirect(this.loginFormUrl);
		} catch (AuthorizationException e) { 
			// 403 - Forbidden - The user might be logged in but does not have the necessary permissions for the resource
			httpResponse.sendError(403, "User Not Authorized");
		} catch (Exception e) {
			// 400 - Bad request.
			httpResponse.sendError(400, "Bad request"); 
		}
	}

	@Override
	public void destroy() {
		
	}

	private ActionRequest getActionRequestFromUrl(String url) {
		ActionRequest result = null;
		if(url!=null) {
			String search = new String(url.split("\\?")[0]);
			if(search.startsWith(this.securityResourcesPrefix)) {
				search = search.substring(this.securityResourcesPrefix.length());
			}
			while(result==null && search.length()>0) {
				if(securityResources.containsKey(search)) {
					result = new ActionRequest(search, securityResources.getProperty(search));
				} else {
					search = search.substring(0, search.lastIndexOf("/"));
				}
			}
		}
		return result;
	}

}
