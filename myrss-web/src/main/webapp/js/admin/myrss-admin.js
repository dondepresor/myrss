
/** MyRSS Administration log. */
var raLog = true;

/**
 * MyRSS Administration.
 * @param app the MYRSS application.
 */
function MYRSSAdmin(app) {
	if(raLog) console.log("[MYRSSAdmin]");
	this.app = app;
}

/**
 * Loads the view.
 * @param callback method to invoke when finished.
 */
MYRSSAdmin.prototype.init = function(callback) {
	if(raLog) console.log("[MYRSSAdmin][init]");
	
};

/**
 * Clears the view.
 */
MYRSSAdmin.prototype.destroy = function() {
	if(raLog) console.log("[MYRSSAdmin][destroy]");
	
};

/**
 * Resizes the viewer.
 */
MYRSSViewer.prototype.resize = function() {
	if(rvLog) console.log("[MYRSSAdmin][resize]");
//	if(this.ui.hasOwnProperty("layout") && this.ui.layout!=null) {
//		this.ui.layout.setSizes();
//	}
};

