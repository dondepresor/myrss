package com.sc.myrss.web.util;

import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.context.support.ResourceBundleMessageSource;

public class ExposedResourceBundleMessageSource extends ResourceBundleMessageSource {

	public ResourceBundle getbundle(String basename, Locale locale) {
		return getResourceBundle(basename, locale);
	}
}