package com.sc.myrss.web.mvc.handlers;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sc.myrss.web.WebConstants;
import com.sc.myrss.web.mvc.ErrorResponse;
import com.sc.myrss.web.mvc.exception.ValidationException;
import com.sc.myrss.web.mvc.util.I18nHelper;

@ControllerAdvice
public class ControllerExceptionHandler {

	@Autowired
	private I18nHelper i18n;

	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ResponseBody
	protected ErrorResponse handleException(ValidationException e, Locale locale, HttpServletResponse response) throws IOException {
		return new ErrorResponse(HttpStatus.BAD_REQUEST.toString(), e.getMessage());
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	protected ErrorResponse handleException(RuntimeException e, Locale locale, HttpServletResponse response) throws IOException {
		String message = i18n.getString(WebConstants.MSG_ERR_INTERNAL_SERVER_ERROR, locale);
		return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), message);
	}
}
