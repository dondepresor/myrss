package com.sc.myrss.dao.hibernate.vo;

import java.io.Serializable;
import java.util.Objects;

public class HAggregatorSettingsId implements Serializable {

	private static final long serialVersionUID = 4219344155931713524L;

	private String userId;

	private String subscriptionUrl;

	public HAggregatorSettingsId() {}

	public HAggregatorSettingsId(String userId, String subscriptionUrl) {
		this.userId = userId;
		this.subscriptionUrl = subscriptionUrl;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSubscriptionUrl() {
		return subscriptionUrl;
	}

	public void setSubscriptionUrl(String subscriptionUrl) {
		this.subscriptionUrl = subscriptionUrl;
	}

	@Override
	public int hashCode() {
		return Objects.hash(subscriptionUrl, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HAggregatorSettingsId))
			return false;
		HAggregatorSettingsId other = (HAggregatorSettingsId) obj;
		return Objects.equals(subscriptionUrl, other.subscriptionUrl) && Objects.equals(userId, other.userId);
	}

}
