package com.sc.myrss.web.mvc.login;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sc.myrss.model.security.User;
import com.sc.myrss.neg.security.SecurityService;
import com.sc.myrss.web.WebConstants;
import com.sc.myrss.web.mvc.exception.ValidationException;

@RestController
@RequestMapping("/login")
public class LoginController {

	private static final String[] LOGIN_MESSAGE_KEYS = {"pass", "reset", "send", "title", "user"};

	private static final String LOGIN_ERROR ="login_error";

	@Autowired
	private MessageSource loginMsg;

	@Autowired
	private SecurityService securityService;

	@RequestMapping(value="/messages", method=RequestMethod.GET)
	public @ResponseBody Map<String, String> getLoginMessages(Locale locale) {
		Map<String, String> result = new HashMap<String, String>();
		for(String key : LOGIN_MESSAGE_KEYS) {
			result.put(key, loginMsg.getMessage(key,  null, locale));
		}
		return result;
	}

	@RequestMapping(value="/doLogin", method=RequestMethod.POST)
	public @ResponseBody Boolean doLogin(@RequestBody LoginForm form, 
			HttpServletRequest httpServletRequest, Locale locale) throws ValidationException {
		
		HttpSession httpSession = httpServletRequest.getSession(true);
		if(!httpSession.isNew() && httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME)!=null) {
			httpSession.invalidate();
			httpSession = httpServletRequest.getSession(true);
		}
		
		if(securityService.validateLogin(form.getLogin(), form.getPassword())) {
			User user = securityService.getUser(form.getLogin());
			httpSession.setAttribute(WebConstants.USER_BEAN_SESSION_NAME, user);
			if(user.getLang()!=null) {
				Locale userLocale = null;
				try {
					userLocale = new Locale(user.getLang());
				} catch(Exception e) {
					userLocale = locale;
				}
				httpSession.setAttribute(WebConstants.LOCALE_BEAN_SESSION_NAME, userLocale);
			}
			return true;
		} else {
			throw new ValidationException(loginMsg.getMessage(LOGIN_ERROR, null, locale));
		}
	}

}
