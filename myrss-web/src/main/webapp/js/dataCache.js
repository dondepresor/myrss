
/** DataCache logs. */
var dcLog = false;

/**
 * A DataCache retrieves and caches asynchronous information about items.
 * 
 * Operation:
 * ==========
 * 
 * A DataCache stores a loader function (loadMethod) for caching data, and a map of requested items.
 * The first request for an item triggers a request to the loader function to get the data.
 * Said request gets stored, along with subsequent requests for the same item, until the item is retrieved.
 * Then, all the cached pending requests are fulfilled.
 * Finally, all new requests for the same item will be served immediatly with the cached data.
 * 
 * Construction:
 * =============
 * 
 * Create a new DataCache object, passing the loadMethod function as only attribute.
 * 
 * The loadMethod function:
 * 
 * let loadMethod = function(key, callback);
 * 
 * Is an asynchronous function, which will fetch the requested data.
 * Once the data is loaded, or in case of error, the method invokes a callback
 * with the operation result. Said result conforms to the next data structure:
 * 
 * operationResult: {
 * 	success: boolean, whether the item could be retrieved
 * 	value: the retrieved item, or null in case of error
 * 	error: the error message, when an error occured
 * }
 * 
 * The load helper functions:
 * ==========================
 * 
 * Get helper function:
 * 
 * This function receives an url, performs the GET request, and returns the operationResult data structure.
 * It can be used from the loadMethod to easy the coding process (see example).
 * 
 * Post helper function:
 * 
 * Same as before, but receives a POST payload.
 * 
 * Requesting data:
 * ================
 * 
 * To request for items, invoke the runWithCache function.
 * The first argument is the item key.
 * The second argument is the callback function.
 * 
 * The callback function will be invoked when the item is loaded, or immediatly if there
 *  is already a result. It will receive the same operationResult as only argument.
 * 
 * let callbackFunction = function(operationResult); 
 * 
 * An example:
 * ===========
 * 
 * 	// Define the downloader function, using the load helper
 * 	let myLoaderFunction = function(key, callback) {
 * 		DataCache.getHelper("myDomain/myRequest?param="+ key, callback);
 * 	};
 * 	
 * 	// Create the cache object
 * 	let myCache = new DataCache("myCache", myLoaderFunction);
 * 
 * 	// I want to run this code with the cached item
 * 	let doWithItem = function(itemData) {
 * 		if(itemData.success) {
 * 			let data = itemData.value;
 * 			// do things
 * 		} else {
 * 			let error = itemData.error;
 * 			// do different things
 * 		}
 * 	};
 * 	
 * 	// Or this other different code
 * 	let doOtherThings = function(itemData) {
 * 		// Same data type, different operations
 * 	};
 * 	
 * 	// So i start requesting my items, passing 
 *  // along the function to run with the results:
 * 	myCache.runWithCache("key1", doWithItem);
 * 	myCache.runWithCache("key2", doWithItem);
 * 	myCache.runWithCache("key1", doOtherThings);
 * 	myCache.runWithCache("key3", doOtherThings);
 * 	
 */
function DataCache(id, loadMethod) {
	if(dcLog) console.log("[DataCache](id: "+ id +")");
	this.map = {};
	this.id = id;
	this.loadMethod = loadMethod;
};

DataCache.getHelper = function(key, url, callback) {
	if(dcLog) console.log("[DataCache.getHelper](key: "+ key +", url: "+ url +")");
	$.ajax({
		method: "GET",
		url: url,
		dataType: "json",
		success: $.proxy(function(data) {
			if(dcLog) console.log("[DataCache.getHelper] success!");
			try {
				if(this.callback!=null) {
					this.callback(this.key, {
						success: true,
						value: data,
						error: null
					});
				}
			} catch(e) {
				if(dcLog) console.log("[DataCache.getHelper] Error processing success response. Message: "+ e);
			}
		}, {key: key, callback: callback || null}),
		error: $.proxy(function(xhr, ts, mesage) {
			if(dcLog) console.log("[DataCache.getHelper] error!");
			try {
				if(this.callback!=null) {
					this.callback(this.key, {
						success: false,
						value: null,
						error: mesage
					});
				}
			} catch(e) {
				if(dcLog) console.log("[DataCache.getHelper] Error processing error response. Message: "+ e);
			}
		}, {key: key, callback: callback || null})
	});
};

DataCache.postHelper = function(key, url, payload, callback) {
	if(dcLog) console.log("[DataCache.postHelper](key: "+ key +", url: "+ url +")");
	$.ajax({
		method: "POST",
		url: url,
		contentType: 'application/json',
		data: JSON.stringify(payload),
		dataType: "json",
		success: $.proxy(function(data) {
			if(dcLog) console.log("[DataCache.postHelper] success!");
			try {
				if(this.callback!=null) {
					this.callback(this.key, {
						success: true,
						value: data,
						error: null
					});
				}
			} catch(e) {
				if(dcLog) console.log("[DataCache.postHelper] Error processing success response. Message: "+ e);
			}
		}, {key: key, callback: callback || null}),
		error: $.proxy(function(xhr, ts, message) {
			if(dcLog) console.log("[DataCache.postHelper] error!");
			try {
				if(this.callback!=null) {
					this.callback(this.key, {
						success: false,
						value: null,
						error: message
					});
				}
			} catch(e) {
				if(dcLog) console.log("[DataCache.postHelper] Error processing error response. Message: "+ e);
			}
		}, {key: key, callback: callback || null})
	});
};

let dataCacheMethods = {
	
	runWithCache: function(key, method) {
		if(dcLog) console.log("[DataCache](id: "+ this.id +").runWithCache(key: "+ key +")");
		if(!this.map.hasOwnProperty(key)) {
			this.map[key] = {
				key: key,
				loaded: false,
				response: null,
				methods: [method]
			};
			this.loadMethod(key, $.proxy(this._entryLoaded, this));
		} else {
			let entry = this.map[key];
			if(entry.loaded) {
				method(entry.response);
			} else {
				entry.methods.push(method);
			}
		}
	},
	
	_entryLoaded: function(key, result) {
		if(dcLog) console.log("[DataCache](id: "+ this.id +").entryLoaded(key: "+ key +")");
		let entry = this.map[key];
		entry.response = result;
		entry.loaded = true;
		for(let method of entry.methods) {
			method(result);
		}
	}
};

for(let name in dataCacheMethods) {
	DataCache.prototype[name] = dataCacheMethods[name];
}
