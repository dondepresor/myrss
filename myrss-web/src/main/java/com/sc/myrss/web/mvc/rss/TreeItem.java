package com.sc.myrss.web.mvc.rss;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TreeItem implements Serializable {

	private static final long serialVersionUID = -2140369057430767877L;

	private String id;

	private String text;

	private boolean checked;

	private String checkbox;

	private TreeItemIcons icons;

	private List<TreeItem> items = new ArrayList<TreeItem>();

	private Map<String, String> userdata;

	public TreeItem() {}

	public TreeItem(String id, String text) {
		this.id = id;
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getCheckbox() {
		return checkbox;
	}

	public void setCheckbox(String checkbox) {
		this.checkbox = checkbox;
	}

	public TreeItemIcons getIcons() {
		return icons;
	}

	public void setIcons(TreeItemIcons icons) {
		this.icons = icons;
	}

	public Map<String, String> getUserdata() {
		return userdata;
	}

	public void setUserdata(Map<String, String> userdata) {
		this.userdata = userdata;
	}

	public List<TreeItem> getItems() {
		return items;
	}

	public void setItems(List<TreeItem> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "TreeItem [id=" + id + ", text=" + text + ", checked=" + checked + ", checkbox=" + checkbox + ", icons="
				+ icons + ", userdata=" + userdata + ", items=" + items + "]";
	}
}
