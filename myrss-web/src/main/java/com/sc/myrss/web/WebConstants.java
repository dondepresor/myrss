package com.sc.myrss.web;

public class WebConstants {

	public static final String ALL_CATEGORY = "ALL";

	public static final String USER_BEAN_SESSION_NAME = "user";

	public static final String LOCALE_BEAN_SESSION_NAME = "user_locale";

	public static final String MSG_ERR_NO_PERMISSION = "msg_err_unauthorized";

	public static final String MSG_ERR_UNAUTHORIZED = "msg_err_not_found";

	public static final String MSG_ERR_INTERNAL_SERVER_ERROR = "msg_err_internal_server_error";

	public static final String MSG_ERR_INVALID_REQUEST = "msg_err_invalid_request";

	public static final String ACCESS_SECURITY_RESOURCE = "access";

	public static final String BROWSE_SECURITY_RESOURCE = "browse";

	public static final String ADMIN_SECURITY_RESOURCE = "admin";

	public static final Integer DEFAULT_PAGE_SIZE = 10;
}
