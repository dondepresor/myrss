package com.sc.myrss.model.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Role implements Serializable {

	private static final long serialVersionUID = 5076366709938922929L;

	private String id;

	private String name;

	private List<SecResource> resources = new ArrayList<SecResource>();

	public SecResource getSecResourceById(String secResource) {
		for(SecResource item : resources) {
			if(item.getId().equals(secResource)) {
				return item;
			}
		}
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SecResource> getResources() {
		return resources;
	}

	public void setResources(List<SecResource> resources) {
		this.resources = resources;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", resources=" + resources + "]";
	}

}
