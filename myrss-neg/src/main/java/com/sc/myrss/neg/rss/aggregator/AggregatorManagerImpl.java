package com.sc.myrss.neg.rss.aggregator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import com.sc.myrss.dao.AggregatorDAO;
import com.sc.myrss.dao.ConfigDAO;
import com.sc.myrss.dao.RssDAO;
import com.sc.myrss.dao.factory.AggregatorDAOFactory;
import com.sc.myrss.dao.factory.ConfigDAOFactory;
import com.sc.myrss.dao.factory.RssDAOFactory;
import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.model.rss.aggregator.FeedAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.ServerAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.UserAggregatorSettings;
import com.sc.myrss.model.security.User;
import com.sc.myrss.neg.rss.downloader.exception.BootException;

@Service
public class AggregatorManagerImpl implements AggregatorManager {

	private static final Logger log = Logger.getLogger(AggregatorManagerImpl.class);

	@Autowired
	private RssDAOFactory rssDAOFactory;

	@Autowired
	private ConfigDAOFactory configDAOFactory;

	@Autowired
	private AggregatorDAOFactory aggregatorDAOFactory;

	private List<FeedAggregator> aggregators = null;

	private List<ScheduledFuture<?>> futures = null;

	private ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();

	private boolean isStarted = false;

	@Override
	public void start() {
		if(aggregators!=null) {
			taskScheduler.initialize();
			log.debug("[start] Performing a first complete download");
			new Thread(new FirstRun(aggregators), "First download run thread").start();
			log.debug("[start] Now scheduling future downloads");
			futures = new ArrayList<ScheduledFuture<?>>();
			boolean anyTaskCreated = false;
			for(FeedAggregator task : aggregators) {
				futures.add(taskScheduler.schedule(task, new CronTrigger(task.getCronString())));
				anyTaskCreated = true;
			}
			isStarted = anyTaskCreated;
		}
	}

	@Override
	public void stop() {
		if(isStarted && futures!=null) {
			for(ScheduledFuture<?> future : futures) {
				future.cancel(true);
			}
			futures = null;
			aggregators = null;
		}
		isStarted = false;
	}

	@Override
	public void configure() {
		if(isStarted) {
			throw new BootException("Cannot configure aggregators after manager has been started");
		} else {
			RssDAO rssDAO = rssDAOFactory.getDAO();
			ConfigDAO configDAO = configDAOFactory.getDAO();
			AggregatorDAO aggregatorDAO = aggregatorDAOFactory.getDAO();
			Map<String, List<Subscription>> subscriptionsByCronExpression = new HashMap<String, List<Subscription>>();
			Map<String, Map<Subscription, Long>> guardTimesBySubscriptionByCronExpression = new HashMap<String, Map<Subscription, Long>>();
			ServerAggregatorSettings sas = configDAO.getServerAggregatorSettings();
			List<User> browserUsers = aggregatorDAO.listBrowserUsers();
			for(User user : browserUsers) {
				UserAggregatorSettings userSettings = configDAO.getUserAggregatorSettings(user.getLogin());
				if(userSettings==null) userSettings = new UserAggregatorSettings(user.getLogin(),
					sas.getFetchRateCronExpression(), sas.getReadItemsGuardTime());
				for(Subscription subscription :rssDAO.getUsersSubscriptions(user.getLogin())) {
					FeedAggregatorSettings subscriptionSettings = 
							configDAO.getFeedAggregatorSettings(user.getLogin(), subscription.getUrl());
					if(subscriptionSettings==null) subscriptionSettings = 
						new FeedAggregatorSettings(user.getLogin(), subscription.getUrl(),
							userSettings.getFetchRateCronExpression(), userSettings.getReadItemsGuardTime());
					String cron = subscriptionSettings.getFetchRateCronExpression();
					if(!subscriptionsByCronExpression.containsKey(cron)) {
						subscriptionsByCronExpression.put(cron, new ArrayList<Subscription>());
						guardTimesBySubscriptionByCronExpression.put(cron, new HashMap<Subscription, Long>());
					}
					subscriptionsByCronExpression.get(cron).add(subscription);
					guardTimesBySubscriptionByCronExpression.get(cron).put(subscription, subscriptionSettings.getReadItemsGuardTime());
				}
			}
			aggregators = new ArrayList<FeedAggregator>();
			for(String cron : subscriptionsByCronExpression.keySet()) {
				if(log.isDebugEnabled()) {
					log.debug("[configure] Adding FeedAggregator with cron expression "+ cron +" for the subscriptions:");
					for(Subscription subscription : subscriptionsByCronExpression.get(cron)) {
						log.debug("[configure]\tuser: "+ subscription.getUserId() +", url: "+ subscription.getUrl());
					}
				}
				aggregators.add(new FeedAggregator(aggregatorDAO, cron, 
					subscriptionsByCronExpression.get(cron), 
					guardTimesBySubscriptionByCronExpression.get(cron)));
			}
		}
	}

	private class FirstRun implements Runnable {
		private List<FeedAggregator> aggregators = null;
		public FirstRun(List<FeedAggregator> aggregators) {
			this.aggregators = aggregators;
		}
		public void run() {
			for(FeedAggregator task : aggregators) {
				task.run();
			}
		}
	}
}
