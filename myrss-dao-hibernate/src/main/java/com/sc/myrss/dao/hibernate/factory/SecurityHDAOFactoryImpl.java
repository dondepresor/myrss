package com.sc.myrss.dao.hibernate.factory;

import org.springframework.stereotype.Component;

import com.sc.myrss.dao.SecurityDAO;
import com.sc.myrss.dao.factory.SecurityDAOFactory;
import com.sc.myrss.dao.hibernate.SecurityHDAOImpl;

@Component
public class SecurityHDAOFactoryImpl implements SecurityDAOFactory {

	@Override
	public SecurityDAO getDAO() {
		return new SecurityHDAOImpl();
	}

}
