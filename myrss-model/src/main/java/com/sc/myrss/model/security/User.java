package com.sc.myrss.model.security;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = 5971282082478712905L;

	private String login;

	private String name;

	private String hash;

	private Role role;

	private String lang;

	private boolean nightMode;

	public boolean can(String secResource) {
		return 
			role!=null && 
			role.getSecResourceById(secResource)!=null;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isNightMode() {
		return nightMode;
	}

	public void setNightMode(boolean nightMode) {
		this.nightMode = nightMode;
	}

	@Override
	public String toString() {
		return "User [login=" + login + ", name=" + name + ", hash=" + hash + 
			", role=" + role + ", lang=" + lang + ", nightMode=" + nightMode + "]";
	}

}
