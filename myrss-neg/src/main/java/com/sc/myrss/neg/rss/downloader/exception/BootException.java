package com.sc.myrss.neg.rss.downloader.exception;

public class BootException extends RuntimeException {

	private static final long serialVersionUID = 1700884441051441048L;

	public BootException() {
		super();
	}

	public BootException(String message) {
		super(message);
	}

	public BootException(String message, Throwable cause) {
		super(message);
		initCause(cause);
	}
}
