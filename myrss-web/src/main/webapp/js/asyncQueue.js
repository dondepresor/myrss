
/** AsyncQueue logs. */
var aqLog = false;

/**
 * An asynchronous queue executes a queue of asynchoronus functions.
 */
function AsyncQueue() {
	if(aqLog) console.log("[AsyncQueue]");
	this.CALL_TIMEOUT = 100;
	this.queue = [];
	this.callback = null;
}

/**
 * Enqueues a function to be executed.
 * The functions first argument must be a callback, and must be invoked 
 * when the async operation has finished, even in case of error.
 * @param aFunction the function to enqueue.
 * @param aContext the context the function runs in.
 * @param moreArgs optional, more arguments to pass to the function.
 * @returns this.
 */
AsyncQueue.prototype.enqueue = function(aFunction, aContext, moreArgs) {
	if(aqLog) console.log("[AsyncQueue][enqueue]");
	let parameters = [$.proxy(this._step, this)];
	if(arguments.length>2) {
		for(let c=2; c<arguments.length; c++) {
			parameters.push(arguments[c]);
		}
	}
	this.queue.push({
		code: aFunction,
		ctx: aContext,
		args: parameters
	});
	return this;
}

/**
 * When the last function in the queue has been executed, 
 * it is time for the callback to be invoked. The callback has
 * no parameters.
 * It is the way of knowing that the queue has been processed.
 * @param aFunction the callback to be invoked after the queue
 * has been processed.
 * @returns this.
 */
AsyncQueue.prototype.setCallback = function(aFunction) {
	if(aqLog) console.log("[AsyncQueue][setCallback]");
	this.callback = aFunction;
	return this;
}

/**
 * Commences the queue processing.
 */
AsyncQueue.prototype.start = function() {
	if(aqLog) console.log("[AsyncQueue][start]");
	this._step();
}

AsyncQueue.prototype._step = function() {
	if(aqLog) console.log("[AsyncQueue][step]");
	if(this.queue.length>0) {
		let def = this.queue.shift();
		window.setTimeout(def.code.apply(def.ctx, def.args), this.CALL_TIMEOUT);
	} else {
		if(this.callback!=null) {
			this.callback();
		}
	}
}
