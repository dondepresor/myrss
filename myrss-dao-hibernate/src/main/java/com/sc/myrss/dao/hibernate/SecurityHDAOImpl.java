package com.sc.myrss.dao.hibernate;

import com.sc.myrss.dao.SecurityDAO;
import com.sc.myrss.model.security.SecResource;
import com.sc.myrss.model.security.User;

public class SecurityHDAOImpl extends BaseHDAO implements SecurityDAO {

	@Override
	public boolean existsUser(String login) {
		return runSingleSelectQuery(
			"SELECT COUNT(*) "+
			"FROM User user "+
			"WHERE UPPER(user.login) LIKE UPPER(:login)", 
			Long.class,
			"login", login).intValue()>0;
	}

	@Override
	public User loadUser(String login) {
		User user = runSingleSelectQuery(
			"SELECT user "+
			"FROM User user "+
			"WHERE UPPER(user.login) LIKE UPPER(:login)", 
			User.class,
			"login", login);
		if(user.getRole()!=null) {
			user.getRole().setResources(runListQuery(
				"SELECT secResource "+
				"FROM SecResource secResource "+
				"WHERE secResource.id IN ("+
					"SELECT rsr.secResourceId "+
					"FROM HRoleSecResource rsr "+
					"WHERE rsr.roleId = :roleId"+
				")", 
				SecResource.class,
				"roleId", user.getRole().getId()));
		}
		return user;
	}

}
