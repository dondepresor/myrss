package com.sc.myrss.model.rss;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Subscription implements Serializable {

	private static final long serialVersionUID = 6801117700222512385L;

	private String userId;

	private String url;

	private String title;

	private String link;

	private String description;

	private String category;

	private String categories;

	private String copyright;

	private String generator;

	private String icon;

	private List<Article> articles;

	private SyndicationType syndicationTpye;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getGenerator() {
		return generator;
	}

	public void setGenerator(String generator) {
		this.generator = generator;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public SyndicationType getSyndicationTpye() {
		return syndicationTpye;
	}

	public void setSyndicationTpye(SyndicationType syndicationTpye) {
		this.syndicationTpye = syndicationTpye;
	}

	@Override
	public String toString() {
		return "Subscription [userId=" + userId + ", url=" + url + ", title=" + title + ", link=" + link
				+ ", description=" + description + ", category=" + category + ", categories=" + categories
				+ ", copyright=" + copyright + ", generator=" + generator + ", icon=" + icon + ", articles=" + articles
				+ ", syndicationTpye=" + syndicationTpye + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(url, userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Subscription))
			return false;
		Subscription other = (Subscription) obj;
		return Objects.equals(url, other.url) && Objects.equals(userId, other.userId);
	}

}