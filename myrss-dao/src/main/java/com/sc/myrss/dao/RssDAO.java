package com.sc.myrss.dao;

import java.util.List;

import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;

public interface RssDAO {

	public List<Subscription> getUsersSubscriptions(String userId);

	public Integer countSubscriptionPendingArticles(String userId, String url);

	public Integer countCategoryPendingArticles(String userId, String category);

	public Integer countPendingArticles(String userId);

	public List<Long> getUserArticleIds(String userId, boolean unreadOnly);

	public List<Long> getSubscriptionArticleIds(String userId, String url, boolean unreadOnly);

	public List<Long> getCategoryArticleIds(String userId, String category, boolean unreadOnly);

	public List<Article> getArticlesByIds(List<Long> ids);

	public void markArticleAsRead(Long readId, boolean readValue);

	public void markSubscriptionAsRead(String userId, String url, boolean readValue);

	public void markCategoryAsRead(String userId, String categoryName, boolean readValue);

	public void markUserAsRead(String userId, boolean readValue);

	public Subscription getSubscription(String userId, String url);

}
