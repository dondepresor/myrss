package com.sc.myrss.neg.rss.reader;

import java.util.List;

import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;

public interface RssService {

	public List<Subscription> getSubscriptions(String login);

	public Subscription getSubscription(String userId, String subscriptionUrl);

	public List<Long> getSubscriptionArticleIds(String userId, String subscriptionUrl, boolean unreadOnly);

	public List<Long> getCategoryArticleIds(String userId, String categoryName, boolean unreadOnly);

	public List<Article> getArticlesByIds(List<Long> ids);

	public Integer getSubscriptionUnreadCount(String userId, String subscriptionUrl);

	public Integer getCategoryUnreadCount(String userId, String categoryName);

	public void markArticleAsRead(Long readId, boolean readValue);

	public void markSubscriptionAsRead(String userId, String subscriptionUrl, boolean readValue);

	public void markCategoryAsRead(String userId, String categoryName, boolean readValue);

}
