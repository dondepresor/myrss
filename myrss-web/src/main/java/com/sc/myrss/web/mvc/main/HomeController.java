package com.sc.myrss.web.mvc.main;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sc.myrss.model.security.SecResource;
import com.sc.myrss.model.security.User;
import com.sc.myrss.web.WebConstants;
import com.sc.myrss.web.mvc.util.I18nHelper;

@RestController
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private I18nHelper i18n;

	@RequestMapping(value="/user", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody WebUser getUser(Locale locale, HttpSession httpSession) {
		return parseUser((User)httpSession.getAttribute(WebConstants.USER_BEAN_SESSION_NAME));
	}

	@RequestMapping(value="/messages", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	public Map<String, String> getMessages(Locale locale, HttpSession httpSession) {
		Locale userLocale = (Locale) httpSession.getAttribute(WebConstants.LOCALE_BEAN_SESSION_NAME);
		return i18n.bundleAsMap(userLocale);
	}

	@RequestMapping(value="/doLogout", method=RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody void doLogout(Locale locale, HttpSession httpSession) {
		httpSession.invalidate();
	}

	private WebUser parseUser(User user) {
		WebUser result = new WebUser();
		result.setLogin(user.getLogin());
		result.setName(user.getName());
		result.setLang(user.getLang());
		result.setNightMode(user.isNightMode());
		result.setRole(user.getRole().getName());
		for(SecResource resource : user.getRole().getResources()) {
			result.getSecurityResources().add(resource.getId());
		}
		return result;
	}
}
