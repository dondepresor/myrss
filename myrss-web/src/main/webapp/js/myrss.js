
/** MyRSSApp logs. */
var mrLog = false;

/**
 * MyRss main application.
 */
function MyRSSApp() {
	if(mrLog) console.log("[MyRSSApp]");
	this.user = null;
	this.messages = null;
	this.views = {};
	this.view = null;
	this.ui = {};
}

/**
 * Init the application.
 */
MyRSSApp.prototype.init = function() {
	if(mrLog) console.log("[MyRSSApp][init]");
	
	// Load views
	try {
		this.views["viewer"] = new MYRSSViewer(this);
	} catch(e) {}
	try {
		this.views["config"] = new MYRSSAdmin(this);
	} catch(e) {}
	
	// Init ui
	new AsyncQueue().
		enqueue(this.fetchUser, this).
		enqueue(this.fetchMessages, this).
		setCallback($.proxy(this.initUI, this)).
		start();
};

/**
 * Loads the logged-in user.
 * @param callback method to invoke when finished.
 */
MyRSSApp.prototype.fetchUser = function(callback) {
	if(mrLog) console.log("[MyRSSApp][fetchUser]");
	let payload = {
		view: this,
		callback: callback
	};
	$.ajax({
		method: "GET",
		url: "mvc/home/user",
		dataType: "json",
		success: $.proxy(function(data) {
			this.view.user = data;
			this.callback();
		}, payload),
		error: $.proxy(function(xhr) {
			if(this.view.processErrorResponse(xhr)) {
				this.callback();
			}
		}, payload)
	});
};

/**
 * Loads the internationalization messages.
 * @param callback method to invoke when finished.
 */
MyRSSApp.prototype.fetchMessages = function(callback) {
	if(mrLog) console.log("[MyRSSApp][fetchMessages]");
	let payload = {
		view: this,
		callback: callback
	};
	$.ajax({
		method: "GET",
		url: "mvc/home/messages",
		dataType: "json",
		success: $.proxy(function(data) {
			this.view.messages = data;
			this.callback();
		}, payload),
		error: $.proxy(function(xhr) {
			if(this.view.processErrorResponse(xhr)) {
				this.callback();
			}
		}, payload)
	});
};

/**
 * Init the user interface.
 * @param callback methiod to invoke when finished.
 */
MyRSSApp.prototype.initUI = function(callback) {
	if(mrLog) console.log("[MyRSSApp][initUI]");
	this.ui.jbody = $(document.body);
	
	// Header:
	let header = $("<div id=\"header\"></div>");
	this.ui.jbody.append(header);
	
	// Main link:
	let mainLink = $("<div id=\"mainLink\"><span>MYRSS</span></div>");
	mainLink.click(function() {window.location.replace("/myrss/")});
	header.append(mainLink);
	
	// Button panel:
	let buttonPanel = $("<div id=\"buttonPanel\"></div>");
	header.append(buttonPanel);
	
	// Config button:
	if(this.user.securityResources.includes("admin")) {
		let configLink = $("<span class=\"config\"><i class=\"fa fa-3x fa-wrench\"></i></span>");
		configLink.attr("title", this.translateText("btn_config")).click($.proxy(this.navigate, this, "config"));
		buttonPanel.append(configLink);
	}
	// Read button:
	if(this.user.securityResources.includes("browse")) {
		let readLink = $("<span class=\"readLink\"><i class=\"fa fa-3x fa-book\"></i></span>");
		readLink.attr("title", this.translateText("btn_read")).click($.proxy(this.navigate, this, "viewer"));
		buttonPanel.append(readLink);
	}
	
	// User panel:
	let userPanel = $("<div id=\"userPanel\"></div>");
	header.append(userPanel);
	
	let themeButton = $("<div class=\"themeButton\"></div>");
	this.themeButton = new ToggleButton({
		jelement: themeButton,
		onClasses: "themeButton on fa fa-2x fa-sun-o",
		offClasses: "themeButton off fa fa-2x fa-moon-o",
		onTooltip: this.translateText("btn_light_theme"),
		offTooltip: this.translateText("btn_dark_theme"),
		initialState: this.user.nightMode,
		onToggle: $.proxy(function() {
			if(this.themeButton.getState()) {
				this.ui.jbody.addClass("dark");
			} else {
				this.ui.jbody.removeClass("dark");
			}
		}, this)
	});
	userPanel.append(themeButton);
	
	userPanel.append($("<span class=\"username\" />").text(this.user.name));
	userPanel.append($("<span class=\"role\" />").text(this.user.role));
	
	let logout = $("<span class=\"logout\"><i class=\"fa fa-3x fa-power-off\"></i></span>");
	logout.attr("title", this.translateText("btn_logout")).click($.proxy(this.logout, this));
	userPanel.append(logout);
	
	// Body:
	this.ui.contents = $("<div id=\"contents\"></div>");
	this.ui.jbody.append(this.ui.contents);
	$(window).resize($.proxy(function() {
		if(this.view!=null) {
			try {
				this.view.resize();
			} catch(e) {}
		}
	}, this));
	
	// Post-init:
	if(this.user.nightMode) {
		this.ui.jbody.addClass("dark");
	}
	if(this.user.securityResources.includes("browse")) {
		this.navigate("viewer");
	}
};

/**
 * Navigates to the specified resource.
 */
MyRSSApp.prototype.navigate = function(resource) {
	if(mrLog) console.log("[MyRSSApp][navigate]");
	if(this.views.hasOwnProperty(resource)) {
		if(this.view!=null) {
			this.view.destroy();
			this.ui.contents.empty();
			this.view = null;
		}
		this.view = this.views[resource];
		this.view.init();
	}
};

/**
 * Logs out from the application.
 */
MyRSSApp.prototype.logout = function() {
	if(mrLog) console.log("[MyRSSApp][logout]");
	$.ajax({
		method: "GET",
		url: "mvc/home/doLogout",
		success: function() {
			window.location.replace("/myrss/");
			window.location.reload();
		},
		error: $.proxy(function(xhr) {
			this.view.processErrorResponse(xhr);
		}, this)
	});
};

/**
 * Processes and responds to an error response.
 * @param xhr the XmlHttpRequest object.
 * @return true if execution has not been interrupted.
 */
MyRSSApp.prototype.processErrorResponse = function(xhr) {
	if(mrLog) console.log("[MyRSSApp][processErrorResponse]");
	if(xhr && xhr!=null) {
		if(xhr.status===401) {
			window.location.replace("./login/login.html");
			return false;
		} else if(xhr.status===403) {
			this.popupMessages([{level: 2, key: "msg_err_unauthorized"}]);
		} else if(xhr.status===404) {
			this.popupMessages([{level: 2, key: "msg_err_not_found"}]);
		} else {
			try {
				let payload = JSON.parse(xhr.responseText);
				this.popupMessages([{level: 2, text: payload.message}]);
			} catch(e) {}
		}
	}
	return true;
};

/**
 * Popups a list of messages.
 * @param messageList a list of message objects.
 */
MyRSSApp.prototype.popupMessages = function(messageList) {
	if(mrLog) console.log("[MyRSSApp][popupMessages]");
	if(messageList && messageList!=null && Array.isArray(messageList)) {
		this.translateMessages(messageList);
		let messageText = "";
		let maxlevel = 0;
		for(let message of messageList) {
			if(message.hasOwnProperty("text")) {
				if(messageText.length>0) {
					messageText += "<br/>";
				}
				if(!message.hasOwnProperty("level")) {
					message.level = -1;
				}
				switch(message.level) {
				case 2: {
					messageText += "<i class=\"fa fa-exclamation-circle\" style=\"color:red;\"></i>&nbsp;";
					if(maxlevel<2) maxlevel = 2;
				} break;
				case 1: {
					messageText += "<i class=\"fa fa-exclamation-triangle\" style=\"color:orange;\"></i>&nbsp;";
					if(maxlevel<1) maxlevel = 1;
				} break;
				case 0:  messageText += "<i class=\"fa fa-info-circle\" style=\"color:blue;\"></i>&nbsp;";break;
				default: messageText += "<i class=\"fa fa-circle\" style=\"color:black;\"></i>&nbsp;";break;
				}
				messageText += message.text;
			}
		}
		dhtmlx.message({
			text: messageText,
			type: "info",
			expire: maxlevel<2? 3000: -1
		});
	}
};

/**
 * Translates all messages in the list.
 * @param messageList the list of messages.
 */
MyRSSApp.prototype.translateMessages = function(messageList) {
	if(mrLog) console.log("[MyRSSApp][translateMessages]");
	if(messageList && messageList!=null && Array.isArray(messageList)) {
		for(let message of messageList) {
			this.translateMessage(message);
		}
	}
};

/**
 * Translates, if necessary, a message.
 * @param message the message.
 */
MyRSSApp.prototype.translateMessage = function(message) {
	if(mrLog) console.log("[MyRSSApp][translateMessage]");
	if(message && message!=null && this.messages!=null) {
		if(message.hasOwnProperty("text") && message.text!=null && message.text.length>0) {
			return;
		}
		if(message.hasOwnProperty("key") && message.key!=null && message.key.length>0) {
			let hasArgs = 
				message.hasOwnProperty("arguments") && 
				message.arguments!=null && 
				Array.isArray(message.arguments);
			if(hasArgs) {
				let translatedArguments = [];
				for(let argument of message.arguments) {
					translatedArguments.push(this.translateText(argument));
				}
				message.arguments = translatedArguments;
			}
			let translation = this.translateText(message.key);
			if(hasArgs) {
				for(let c=0; c<message.arguments.length; c++) {
					translation = translation.replace(new RegExp("\\{"+ c +"\\}", "g"), message.arguments[c]);
				}
			}
			message.text = translation;
		}
	}
};

/**
 * Translates a text.
 * @param text the text.
 */
MyRSSApp.prototype.translateText = function(text) {
	return this.messages.hasOwnProperty(text)? this.messages[text]: text;
};
