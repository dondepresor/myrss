
/**
 * App for the login page.
 */
function LoginApp() {
	this.wins = new dhtmlXWindows("web");
	this.wins.attachViewportTo(document.body);
	this.body = $("body");
	this.body.css("position", "absolute");
	this.messages = {
		title: "MYRSS Login",
		user: "User",
		pass: "Password",
		send: "Send",
		reset: "Reset"
	};
}

/**
 * Load the internationalized messages.
 * @param callback method to invoke when finished.
 */
LoginApp.prototype.requestMessages = function(callback) {
	$.ajax({
		method: "get",
		url: "../mvc/login/messages",
		dataType: "json",
		success: $.proxy(function(data) {
			this.view.messages = data;
			if(this.callback!=null) {
				this.callback();
			}
		}, {view: this, callback: callback || null}),
		error: $.proxy(function(jqXHR, textStatus, errorThrown) {
			if(this.callback!=null) {
				this.callback();
			}
		}, {view: this, callback: callback || null})
	});
};

/**
 * init the page.
 */
LoginApp.prototype.init = function() {
	this.body.addClass("notReady");
	
	window.document.title = this.messages.title;
	
	this.loginWindow = this.wins.createWindow("loginWindow", 20, 20, 400, 180);
	this.loginWindow.centerOnScreen();
	this.loginWindow.denyPark();
	this.loginWindow.denyResize();
	this.loginWindow.button("park").hide();
	this.loginWindow.button("minmax").hide();
	this.loginWindow.button("close").hide();
	this.loginWindow.setText(this.messages.title);
	this.loginWindow.keepInViewport(true);
	this.loginWindow.attachEvent("onClose", function() {return false;});
	this.loginForm = this.loginWindow.attachForm([
		{type: "input", name: "username", label: this.messages.user, labelPosition: "label-left", labelWidth: 120, inputWidth: 200, offsetLeft: 20, required: true},
		{type: "password", name: "password", label: this.messages.pass, labelPosition: "label-left", labelWidth: 120, inputWidth: 200, offsetLeft: 20, required: true},
		{type: "block",name: "buttons", width: 350, offsetTop: 20, list:[
			{type: "button", name: "submit", value: this.messages.send, width: 145},
			{type: "newcolumn", offset: 20},
			{type: "button", name: "reset", value: this.messages.reset, width: 145}
		]}
	]);
	this.loginForm.attachEvent("onButtonClick", $.proxy(function(name) {
		if("reset"===name) {
			this.loginForm.setItemValue("username", "");
			this.loginForm.setItemValue("password", "");
		} else if("submit"===name) {
			let username = this.loginForm.getItemValue("username");
			let password = this.loginForm.getItemValue("password");
			this.doLogin(username, password);
		}
	}, this));
	this.loginForm.attachEvent("onEnter", $.proxy(function() {
		let username = this.loginForm.getItemValue("username");
		let password = this.loginForm.getItemValue("password");
		this.doLogin(username, password);
	}, this));
	
	let icons = "<span class=\"logos\">";
		icons += "<i class=\"fa fa-2x fa-chrome\" style=\"margin-right: 4px;\" />";
		icons += "<i class=\"fa fa-2x fa-firefox\" />";
	icons += "</span>";
	$(document.body).append(icons);
	
	this.animationEndEventListener = $.proxy(function() {
		document.body.removeEventListener("animationend", this.animationEndEventListener, false);
		this.body.removeClass("getReady").addClass("ready");
		this.loginForm.setItemFocus("username");
	}, this);
	document.body.addEventListener("animationend", this.animationEndEventListener, false);
	this.body.removeClass("notReady").addClass("getReady");
};

/**
 * Perform the login.
 */
LoginApp.prototype.doLogin = function(username, password) {
	this.loginForm.lock();
	$.ajax({
		method: "POST",
		url: "../mvc/login/doLogin",
		data: JSON.stringify({
			login: username,
			password: password
		}),
		dataType: "json",
		contentType: "application/json",
		success: $.proxy(function(data) {
			this.loginForm.unlock();
			window.location.replace("/myrss/");
		}, this),
		error: $.proxy(function(jqXHR, textStatus, errorThrown) {
			this.loginForm.unlock();
			try {
				let payload = JSON.parse(jqXHR.responseText);
				this.showError(payload.message);
			} catch(e) {}
		}, this)
	});
};

/**
 * Display an error message.
 */
LoginApp.prototype.showError = function(messageText) {
	dhtmlx.message({
		text: messageText,
		type: "error",
		expire: 3000
	});
}
