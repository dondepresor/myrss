package com.sc.myrss.web.mvc.rss;

import java.io.Serializable;

public class TreeItemIcons implements Serializable {

	private static final long serialVersionUID = -4719996964894866164L;

	private String file;

	private String folder_opened;

	private String folder_closed;

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getFolder_opened() {
		return folder_opened;
	}

	public void setFolder_opened(String folder_opened) {
		this.folder_opened = folder_opened;
	}

	public String getFolder_closed() {
		return folder_closed;
	}

	public void setFolder_closed(String folder_closed) {
		this.folder_closed = folder_closed;
	}

	@Override
	public String toString() {
		return "TreeItemIcons [file=" + file + ", folder_opened=" + folder_opened + ", folder_closed=" + folder_closed
				+ "]";
	}
	
}
