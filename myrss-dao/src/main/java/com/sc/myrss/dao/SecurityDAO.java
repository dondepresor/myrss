package com.sc.myrss.dao;

import com.sc.myrss.model.security.User;

public interface SecurityDAO {

	public boolean existsUser(String login);

	public User loadUser(String login);

}
