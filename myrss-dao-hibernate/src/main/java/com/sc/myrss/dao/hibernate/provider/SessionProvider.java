package com.sc.myrss.dao.hibernate.provider;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class SessionProvider {

	private static final Object SYNCHRONIZATION = new Object();

	private static final String PERSISTENCE_UNIT_NAME = "myrss";

	private static SessionProvider instance = null;

	private EntityManager em = null;

	private SessionFactory sf = null;

	public static SessionProvider getInstance() {
		if(instance==null) {
			synchronized(SYNCHRONIZATION) {
				if(instance==null) {
					instance = new SessionProvider();
				}
			}
		}
		return instance;
	}

	protected EntityManager getEntityManager() {
		if(em==null) {
			synchronized(SYNCHRONIZATION) {
				if(em==null) {
					em = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
				}
			}
		}
		return em;
	}

	protected SessionFactory getSessionFactory() {
		if(sf==null) {
			synchronized (SYNCHRONIZATION) {
				if(sf==null) {
					Session current = getEntityManager().unwrap(Session.class);
					sf = current.getSessionFactory();
				}
			}
		}
		return sf;
	}

	public Session getSession() {
		return getSessionFactory().openSession();
	}
}
