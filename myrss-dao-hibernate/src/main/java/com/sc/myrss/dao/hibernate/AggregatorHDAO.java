package com.sc.myrss.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sc.myrss.dao.AggregatorDAO;
import com.sc.myrss.model.rss.Article;
import com.sc.myrss.model.rss.Subscription;
import com.sc.myrss.model.security.User;

public class AggregatorHDAO extends BaseHDAO implements AggregatorDAO {

	@Override
	public List<User> listBrowserUsers() {
		return runListQuery(
			"SELECT u FROM User u "+
			"WHERE u.role.id IN ("+
				"SELECT rsr.roleId FROM HRoleSecResource rsr "+
				"WHERE rsr.secResourceId = 'browse'"+
			")", User.class);
	}

	@Override
	public void updateSubscription(Subscription subscription) {
		Session s = null;
		Transaction tx = null;
		try {
			s = getSession();
			tx = s.beginTransaction();
			readySubscriptionForSaving(subscription);
			s.saveOrUpdate(subscription);
			tx.commit();
		} catch(Throwable t) {
			if(tx!=null) tx.rollback();
		} finally {
			s.close();
		}
	}

	private void readySubscriptionForSaving(Subscription s) {
		if(s.getTitle()!=null && s.getTitle().length()>100) {
			s.setTitle(s.getTitle().substring(0, 100));
		}
		if(s.getLink()!=null && s.getLink().length()>2048) {
			s.setLink(s.getLink().substring(0, 2048));
		}
		if(s.getDescription()!=null && s.getDescription().length()>256) {
			s.setDescription(s.getDescription().substring(0, 256));
		}
		if(s.getCategory()!=null && s.getCategory().length()>256) {
			s.setCategory(s.getCategory().substring(0, 256));
		}
		if(s.getCategories()!=null && s.getCategories().length()>2048) {
			s.setCategories(s.getCategories().substring(0, 2048));
		}
		if(s.getCopyright()!=null && s.getCopyright().length()>200) {
			s.setCopyright(s.getCopyright().substring(0, 200));
		}
		if(s.getGenerator()!=null && s.getGenerator().length()>200) {
			s.setGenerator(s.getGenerator().substring(0, 200));
		}
		if(s.getIcon()!=null && s.getIcon().length()>2048) {
			s.setIcon(s.getIcon().substring(0, 2048));
		}
	}

	@Override
	public boolean articleExists(Article article) {
		return runSingleSelectQuery(
			"SELECT COUNT(a) FROM Article a "+
			"WHERE a.userId = :userId "+
				"AND a.subscriptionUrl = :subscriptionUrl "+
				"AND a.title = :title "+
				"AND a.contentHash = :contentHash", 
			Long.class,
			"userId", article.getUserId(),
			"subscriptionUrl", article.getSubscriptionUrl(),
			"title", article.getTitle(),
			"contentHash", article.getContentHash()).intValue()>0;
	}

	@Override
	public void addArticle(Article article) {
		Session s = null;
		Transaction tx = null;
		try {
			s = getSession();
			tx = s.beginTransaction();
			readyArticleForSaving(article);
			s.save(article);
			tx.commit();
		} catch(Throwable t) {
			if(tx!=null) tx.rollback();
		} finally {
			s.close();
		}
	}

	private void readyArticleForSaving(Article a) {
		if(a.getContent()!=null && a.getContent().length()>2147483647) {
			a.setContent(a.getContent().substring(0, 2147483647));
		}
		if(a.getAuthors()!=null && a.getAuthors().length()>1024) {
			a.setAuthors(a.getAuthors().substring(0, 1024));
		}
		if(a.getCategories()!=null && a.getCategories().length()>2048) {
			a.setCategories(a.getCategories().substring(0, 2048));
		}
		if(a.getGuid()!=null && a.getGuid().length()>2048) {
			a.setGuid(a.getGuid().substring(0, 2048));
		}
		if(a.getSourceUrl()!=null && a.getSourceUrl().length()>2048) {
			a.setSourceUrl(a.getSourceUrl().substring(0, 2048));
		}
	}

	@Override
	public Long purgeSubscription(Subscription subscription, Long guardTime) {
		Date limitDate = new Date();
		limitDate.setTime(limitDate.getTime() - guardTime);
		Long count = runSingleSelectQuery(
			"SELECT COUNT(a) FROM Article a "+
				"WHERE a.userId = :userId "+
					"AND a.subscriptionUrl = :subscriptionUrl "+
					"AND a.publicationDate < :limitDate "+
					"AND a.read = 1",
				Long.class,
				"userId", subscription.getUserId(),
				"subscriptionUrl", subscription.getUrl(),
				"limitDate", limitDate);
		runUpdateQuery(
			"DELETE FROM Article a "+
			"WHERE a.userId = :userId "+
				"AND a.subscriptionUrl = :subscriptionUrl "+
				"AND a.publicationDate < :limitDate "+
				"AND a.read = 1",
			"userId", subscription.getUserId(),
			"subscriptionUrl", subscription.getUrl(),
			"limitDate", limitDate);
		return count;
	}
}
