package com.sc.myrss.dao.hibernate.factory;

import org.springframework.stereotype.Component;

import com.sc.myrss.dao.RssDAO;
import com.sc.myrss.dao.factory.RssDAOFactory;
import com.sc.myrss.dao.hibernate.RssHDAOImpl;

@Component
public class RssHDAOFactoryImpl implements RssDAOFactory {

	@Override
	public RssDAO getDAO() {
		return new RssHDAOImpl();
	}

}
