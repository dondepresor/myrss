
/** ToggleButton logs. */
var tbLog = false;

/**
 * Bi-state button control.
 * 
 * @param config configuration: {
 * - jelement: the jquery (div) element to make the button.
 * - onClasses: the on css classes.
 * - offClasses: the off css classes.
 * - onTooltip: optional, tooltip for the on state.
 * - offTooltip: optional, tooltip for the off state.
 * - onToggle: function to invoke when toggled.
 * - initialState: the initial state. Default: off.
 * }
 * 
 * Api:
 * - setState(state) changes the button state. true: on, false: off.
 * - getState() returns the current button state. true: on, false: off.
 */
function ToggleButton(config) {
	if(tbLog) console.log("[ToggleButton]");
	// Config check
	if(!config || 
			config==null || 
			!config.jelement || 
			!config.onClasses || 
			!config.offClasses || 
			!config.onToggle) {
		throw "Invalid ToggleButton configuration";
	}
	this.config = config;
	this.config.jelement.click($.proxy(this._eventListener, this));
	this.state = false;
	if(!this.config.hasOwnProperty("onTooltip")) this.config.onTooltip = null;
	if(!this.config.hasOwnProperty("offTooltip")) this.config.offTooltip = null;
	if(config.hasOwnProperty("initialState")) {
		this.state = config.initialState;
	}
	this.setState(this.state);
}

/**
 * Gets the current button state.
 * @return the current button state. true: on, false: off.
 */
ToggleButton.prototype.getState = function() {
	if(tbLog) console.log("[ToggleButton][getState]");
	return this.state;
};

/**
 * Changes the button state.
 * @param state the new state. true: on, false: off.
 */
ToggleButton.prototype.setState = function(state) {
	if(tbLog) console.log("[ToggleButton][setState](state: "+ state +")");
	if(arguments.length>0 && (state===true || state===false)) {
		let oldClasses = this.state? this.config.onClasses: this.config.offClasses;
		this.state = state;
		let newClasses = this.state? this.config.onClasses: this.config.offClasses;
		this.config.jelement.removeClass(oldClasses).addClass(newClasses);
		let newTooltip = this.state? this.config.onTooltip: this.config.offTooltip;
		if(newTooltip!=null) {
			this.config.jelement.attr("title", newTooltip);
		} else {
			this.config.jelement.removeAttribute("title");
		}
	}
};

ToggleButton.prototype._eventListener = function(evt) {
	if(tbLog) console.log("[ToggleButton][_eventListener]");
	this.setState(!this.getState(), false);
	this.config.onToggle();
};
