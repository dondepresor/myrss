package com.sc.myrss.dao;

import java.util.List;

import com.sc.myrss.model.rss.aggregator.AggregatorSettings;
import com.sc.myrss.model.rss.aggregator.FeedAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.ServerAggregatorSettings;
import com.sc.myrss.model.rss.aggregator.UserAggregatorSettings;

public interface ConfigDAO {

	public ServerAggregatorSettings getServerAggregatorSettings();

	public void saveServerAggregatorSettings(ServerAggregatorSettings settings);

	public UserAggregatorSettings getUserAggregatorSettings(String userId);

	public void saveUserAggregatorSettings(UserAggregatorSettings settings);

	public FeedAggregatorSettings getFeedAggregatorSettings(String userId, String url);

	public void saveFeedAggregatorSettings(FeedAggregatorSettings settings);

	public List<AggregatorSettings> listUserAggegatorSettings(String userId);

	public void deleteUserRelatedAggregatorSettings(String userId);

	public void deleteFeedAggregatorSettings(String userId, String url);

}
