package com.sc.myrss.web.mvc.login;

import java.io.Serializable;

public class LoginForm implements Serializable {

	private static final long serialVersionUID = -3980441972630502458L;

	private String login;

	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginForm [login=" + login + ", password?" + (password!=null) + "]";
	}

}
