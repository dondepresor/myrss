package com.sc.myrss.model.rss.aggregator;

public class ServerAggregatorSettings extends AggregatorSettings {

	public ServerAggregatorSettings() {}

	public ServerAggregatorSettings(String fetchRateCronExpression, Long readItemsGuardTime) {
		this.setFetchRateCronExpression(fetchRateCronExpression);
		this.setReadItemsGuardTime(readItemsGuardTime);
	}
}
