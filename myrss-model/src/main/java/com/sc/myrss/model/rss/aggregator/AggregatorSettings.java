package com.sc.myrss.model.rss.aggregator;

public abstract class AggregatorSettings {

	private String fetchRateCronExpression;

	private Long readItemsGuardTime;

	public String getFetchRateCronExpression() {
		return fetchRateCronExpression;
	}

	public void setFetchRateCronExpression(String fetchRateCronExpression) {
		this.fetchRateCronExpression = fetchRateCronExpression;
	}

	public Long getReadItemsGuardTime() {
		return readItemsGuardTime;
	}

	public void setReadItemsGuardTime(Long readItemsGuardTime) {
		this.readItemsGuardTime = readItemsGuardTime;
	}

}
