package com.sc.myrss.neg.rss.downloader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.annotation.Contract;
import org.apache.http.annotation.ThreadingBehavior;
import org.apache.http.client.HttpResponseException;
import org.apache.http.impl.client.AbstractResponseHandler;
import org.mozilla.universalchardet.UniversalDetector;


@Contract(threading = ThreadingBehavior.IMMUTABLE)
public class EncodingAwareStringResponseHandler extends AbstractResponseHandler<String> {

	private static final String DEFAULT_ENCODING = "UTF-8";

	@Override
	public String handleEntity(HttpEntity entity) throws IOException {
		byte[] data = clone(entity.getContent());
		String encoding = entity.getContentEncoding()==null? null: entity.getContentEncoding().getValue();
		if(encoding==null) encoding = detectEncoding(data);
		if(encoding==null) encoding = DEFAULT_ENCODING;
		if(!Charset.availableCharsets().containsKey(encoding)) encoding = DEFAULT_ENCODING;
		return new String(data, Charset.forName(encoding));
	}

	@Override
	public String handleResponse(final HttpResponse response) throws HttpResponseException, IOException {
		return super.handleResponse(response);
	}

	private byte[] clone(InputStream stream) throws IOException {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;
		while((len=stream.read(buffer))!=-1) {
			result.write(buffer, 0 ,len);
		}
		return result.toByteArray();
	}

	private String detectEncoding(byte[] data) {
		String result = null;
		UniversalDetector detector = new UniversalDetector(null);
		detector.handleData(data, 0, data.length);
		detector.dataEnd();
		result = detector.getDetectedCharset();
		return result;
	}
}
