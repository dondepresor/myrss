package com.sc.myrss.model.rss.aggregator;

public class UserAggregatorSettings extends AggregatorSettings {

	public UserAggregatorSettings() {}

	public UserAggregatorSettings(String userId, 
			String fetchRateCronExpression, Long readItemsGuardTime) {
		this.userId = userId;
		this.setFetchRateCronExpression(fetchRateCronExpression);
		this.setReadItemsGuardTime(readItemsGuardTime);
	}

	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
