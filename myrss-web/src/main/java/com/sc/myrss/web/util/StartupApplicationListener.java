package com.sc.myrss.web.util;

import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.sc.myrss.neg.rss.aggregator.AggregatorManager;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger log = Logger.getLogger(StartupApplicationListener.class);

	@Autowired
	private AggregatorManager aggregatorManager;

	private boolean boot = true; // Wait, what?

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		try {
			if(boot) {
				aggregatorManager.configure();
				aggregatorManager.start();
			}
		} catch(Throwable t) {
			log.error("Error during startup", t);
		}
	}

	@PreDestroy
	public void onDestroy() {
		try {
			aggregatorManager.stop();
		} catch(Throwable t) {
			log.error("Error during shutdown", t);
		}
	}

}
