package com.sc.myrss.dao.factory;

import com.sc.myrss.dao.RssDAO;

public interface RssDAOFactory {

	public RssDAO getDAO();
}
