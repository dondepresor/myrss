package com.sc.myrss.neg.rss.downloader;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.annotation.Contract;
import org.apache.http.annotation.ThreadingBehavior;
import org.apache.http.client.HttpResponseException;
import org.apache.http.impl.client.AbstractResponseHandler;
import org.apache.http.util.EntityUtils;

@Contract(threading = ThreadingBehavior.IMMUTABLE)
public class EncodedStringResponseHandler extends AbstractResponseHandler<String> {

	private String encoding;

	public EncodedStringResponseHandler(String encoding) {
		this.encoding = encoding;
	}

	@Override
	public String handleEntity(final HttpEntity entity) throws IOException {
		return EntityUtils.toString(entity, Charset.forName(encoding));
	}

	@Override
	public String handleResponse(final HttpResponse response) throws HttpResponseException, IOException {
		return super.handleResponse(response);
	}

}